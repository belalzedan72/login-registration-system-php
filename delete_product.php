<?php
include("config_shopping.php");

if (isset($_GET["action"]))
{
    if ($_GET["action"] == "delete")
    {
        foreach ($_SESSION["cart"] as $keys => $value)
        {
            if ($value["product_id"] == $_GET["id"])
            {
                unset($_SESSION["cart"][$keys]);
                echo '<script>alert("Product has been Removed...!")</script>';
                echo '<script>window.location="Shopping_Cart.php"</script>';
            }
        }
    }
}