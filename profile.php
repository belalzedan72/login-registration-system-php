<?php
session_start();
if(!isset($_SESSION["loggedUser"])){
    header('location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Profile</title>
</head>
<body>
    <?php
    include "config.php";
    $email=$_SESSION["loggedUser"];
    $viewUser="SELECT * FROM user WHERE email ='$email'";
    $result= mysqli_query($con,$viewUser);
    if(!$result)
    {
        echo "Error:" . mysqli_error($con);

    }
    $row=mysqli_fetch_array($result);
    $firstName=$row["firstName"];
    $lastName=$row["lastName"];
    $age=$row["age"];
    $mobile = $row ["mobile"];
    ?>
    <h1>User Profile</h1>
    <label >First Name</label>
    <p><?php echo $firstName?></p>

    <label >Last Name</label>
    <p><?php echo $lastName?></p>

    <label >Email</label>
    <p><?php echo $email?></p>

    <label >Age</label>
    <p><?php echo $age?></p>
    
    <label >Mobile</label>
    <p><?php echo $mobile?></p>

    <h4>shopping here</h4>
    <a href="Shopping_Cart.php">🛒Shopping🛒</a>
    <br>
    <br>
    <br>

    <a href="logout.php">Logout</a>
    
</body>
</html>